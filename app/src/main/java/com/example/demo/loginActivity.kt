package com.example.demo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.demo.Retrofit.myRetrofit
import retrofit2.Call
import retrofit2.Response
import java.util.regex.Pattern.compile

class loginActivity : AppCompatActivity() {
    lateinit var email:EditText
    lateinit var password:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
    }
    val pattern =("[a-z0-9]{1,256}" +
            "\\@" +
            "[a-z]{1,10}" +
            "\\." +
            "[a-z]{1,3}")
    fun EmailValid (email:String):Boolean{
        return compile (pattern).matcher(email).matches()}
    fun signin(view : View){
        if (email.text.toString().isEmpty() || password.text.toString().isEmpty()){
            val alert = AlertDialog.Builder(this)
                .setTitle("Ошибка")
                .setMessage("У вас есть незаполненные поля")
                .setPositiveButton("OK",null)
                .create()
                .show()
            return
        }
        if (!EmailValid(email.text.toString())) {
            val alert = AlertDialog.Builder(this)
                .setTitle("Ошибка")
                .setMessage("Email введен неверно")
                .setPositiveButton("OK", null)
                .create()
                .show()
            return
        }
        val retrofit = myRetrofit.getRetrofit()
        val hashMap: HashMap<String, String> = HashMap()
        hashMap["email"] = email.text.toString()
        hashMap["password"] = password.text.toString()
        val call: Call<login> = retrofit.login(hashMap)
        call.enqueue(object: retrofit2.Callback<login>{
            override fun onResponse(call: Call<login>, response: Response<login>) {
               if (response.body()?.token != null){
                   login.userToken = response.body()?.token
                   val intent = Intent(this@loginActivity, MainActivity::class.java)
                   startActivity(intent)
                   finish()
               }else {
                   AlertDialog.Builder(this@loginActivity).setMessage("Неверные данные входа").show()
               }
            }

            override fun onFailure(call: Call<login>, t: Throwable) {
                Toast.makeText(this@loginActivity, t.message, Toast.LENGTH_SHORT).show()
            }

        })
    }
}